
# PruebaAngular
El proyecto fue desarrollado usando maquetación simple HTML / CSS y la interactividad en el desarrollo con tipos de escritura a continuación toda la información del proyecto
Generado con [Angular CLI] (https://github.com/angular/angular-cli) versión 7.3.1.

## Servidor de desarrollo

Corra `ng serve` para un servidor dev. Vaya a `http: // localhost: 4200 /`. La aplicación se volverá a cargar automáticamente si cambia alguno de los archivos de origen.

## Andamios de código

Ejecute `ng generar componente nombre-componente` para generar un nuevo componente. También puede usar `ng genera la directiva | pipe | service | class | guard | interface | enum | module`.

## Construir

Ejecute `ng build` para construir el proyecto. Los artefactos de compilación se almacenarán en el directorio `dist /`. Use la bandera `--prod` para una construcción de producción.

## Ejecución de pruebas unitarias

Ejecute `ng test` para ejecutar las pruebas unitarias a través de [Karma] (https://karma-runner.github.io).

## Ejecutando pruebas de extremo a extremo

Ejecute `ng e2e` para ejecutar las pruebas de extremo a extremo a través de [Protractor] (http://www.protractortest.org/).

## Ayuda adicional

Para obtener más ayuda sobre Angular CLI, use `ng help` o visite el [Angular CLI README] (https://github.com/angular/angular-cli/blob/master/README.md).