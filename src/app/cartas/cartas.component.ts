import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cartas',
  templateUrl: './cartas.component.html',
  styleUrls: ['./cartas.component.css']
})
export class CartasComponent implements OnInit {

  quien:boolean=false;
  items:any;
  items_esp:any=[{
    descripcion:'Item Espanol 1',
    ver:'enlace_ver',
    generar:'enlace_generar',
  },{
    descripcion:'Item Espanol 2',
    ver:'enlace_ver',
    generar:'enlace_generar',
  },{
    descripcion:'Item Espanol 3',
    ver:'enlace_ver',
    generar:'enlace_generar',
  }];

  items_ingl:any=[{
    descripcion:'Item Ingles 1',
    ver:'enlace_ver',
    generar:'enlace_generar',
  },{
    descripcion:'Item Ingles 2',
    ver:'enlace_ver',
    generar:'enlace_generar',
  },{
    descripcion:'Item Ingles 3',
    ver:'enlace_ver',
    generar:'enlace_generar',
  }];

  items_fran:any=[{
    descripcion:'Item Frances 1',
    ver:'enlace_ver',
    generar:'enlace_generar',
  },{
    descripcion:'Item Frances 2',
    ver:'enlace_ver',
    generar:'enlace_generar',
  },{
    descripcion:'Item Frances 3',
    ver:'enlace_ver',
    generar:'enlace_generar',
  }];

  items_ital:any=[{
    descripcion:'Item Italiano 1',
    ver:'enlace_ver',
    generar:'enlace_generar',
  },{
    descripcion:'Item Italiano 2',
    ver:'enlace_ver',
    generar:'enlace_generar',
  },{
    descripcion:'Item Italiano 3',
    ver:'enlace_ver',
    generar:'enlace_generar',
  }];

  items_alem:any=[{
    descripcion:'Item Aleman 1',
    ver:'enlace_ver',
    generar:'enlace_generar',
  },{
    descripcion:'Item Aleman 2',
    ver:'enlace_ver',
    generar:'enlace_generar',
  },{
    descripcion:'Item Aleman 3',
    ver:'enlace_ver',
    generar:'enlace_generar',
  }];

  constructor() { }

  ngOnInit() {
    this.items=this.items_esp;
  }

  onQuien(si:any){
    if(si.target.value=='ESTA'){
      this.quien=true;
    }else{
      this.quien=false;
    }
  }

  onIdioma(si:any){
    switch (si.target.value) {
      case 'espanol':
        this.items=this.items_esp;
      break;
      case 'ingles':
      this.items=this.items_ingl;
      break;
      case 'frances':
      this.items=this.items_fran;
      break;
      case 'italiano':
      this.items=this.items_ital;
      break;
      case 'aleman':
      this.items=this.items_alem;
      break;
      default:
        break;
    }
  }
}
